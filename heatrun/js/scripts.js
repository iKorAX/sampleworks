var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var images = document.getElementsByClassName("slider-img");
    var texts = document.getElementsByClassName("slider-text");
    if (n > images.length){
    	slideIndex = 1;
    } 
    if (n < 1){
    	slideIndex = images.length;
    }
    for (i = 0; i < images.length; i++) {
        images[i].style.display = "none"; 
        texts[i].style.display = "none"; 
    }
    images[slideIndex-1].style.display = "block";
    texts[slideIndex-1].style.display = "block";
}