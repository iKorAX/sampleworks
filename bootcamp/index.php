<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>The News Reporter - We Report News</title>

	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/bebas_regular_macroman/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div class="body-container">
	<header class="page-header">
		<div class="logo-line">

			<div class="logo-menu">
				<a href="index.html"><img src="images/logo.png" class="logo"></a>
			
					<div class="burger">
						<div></div>
						<div></div>
						<div></div>
					</div>
			

				<ul class="header-menu" id="header-menu">
					<li><a href="index.php">Home</a></li>
					<li><a class="popup-link" href="#" show="about-window">About Us</a></li>
					<li><a class="popup-link" href="#" show="contact-window">Contact Us</a></li>
					<li><a class="popup-link" href="#" show="subscribe-window">Subscribe</a></li>
					<li><a class="popup-link" href="#" show="login-window">Login</a></li>
				</ul>
			</div>

			<div id="dark-cover">
				<div class="inpage-window" id="about-window">
						<h2>A brief history of everything</h2>
						<p>Copyright &copy; 2011 The News Reporter Inc. All rights reserved. Theme designed by GraphicsFuel.com</p>
						<p>Reproduction in whole or in part in any form or medium without express written permission of The News Reporter Inc. is prohibited. The trade marks and images used in the design are the copyright of their respective owners. They are used only for display purpose.</p>
				</div>
				<div class="inpage-window" id="contact-window">
					<form>
						<h2>Send us a message</h2>
						<input type="text" name="email" placeholder="Your name">
						<input type="" name="" placeholder="Your e-mail">
						<textarea></textarea>
						<button>Send</button>
					</form>
				</div>

				<div class="inpage-window" id="subscribe-window">
					<form>
						<h2>Subscribe to our daily digest</h2>
						<input type="text" name="email" placeholder="Your e-mail address">
						<input type="text" name="user_name" placeholder="Your name">
						<p>By clicking submit you are agreeing to the Terms and Conditions.</p>
						<button>Subscribe</button>
					</form>
				</div>
				<div class="inpage-window" id="login-window">
					<form>
						<h2>Log in to add content</h2>
						<input type="text" name="email" placeholder="Your username">
						<input type="password" name="user_name" placeholder="Your password">
						<button>Log in</button>
					</form>
				</div>
			</div>

			<div class="social-and-search">
				<div class="social">
					<a href="" class="twitter-link"><i class="icon ion-social-twitter"></i></a>
					<a href="" class="fb-link"><i class="icon ion-social-facebook"></i></a>
					<a href="" class="rss-link"><i class="icon ion-social-rss"></i></a>
					<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=100&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId" width="80" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
					<div class="google-button"><div class="g-plusone" data-size="tall" ... ></div></div>
				</div>

				<div class="search">
					<form class="search-form">
						<input type="text" class="searchbar" name="search">
						<button>
							<i class="icon ion-search"></i>
						</button>
					</form>
				</div>
			</div>
		</div>

		<div class="newsmenu-wrapper">
		<a class="show-categories" href="#">Categories <span>&#9656;</span></a>
			<ul class="horizontalmenu topmenu">
				<li><a href="news-main.php?category=worldnews">World news</a></li>
				<li><a href="news-main.php?category=sports">Sports</a></li>
				<li><a href="news-main.php?category=tech">Tech</a></li>
				<li><a href="news-main.php?category=business">Business</a></li>
				<li><a href="news-main.php?category=movies">Movies</a></li>
				<li><a href="news-main.php?category=entertainment">Entertainment</a></li>
				<li><a href="news-main.php?category=culture">Culture</a></li>
				<li><a href="news-main.php?category=books">Books</a></li>
				<li><a href="news-main.php?category=classifieds">Classifieds</a></li>
				<li><a href="news-main.php?category=blogs">Blogs</a></li>
			</ul>
		</div>
	</header>
	<main>
		<div class="newsdiv">
			<div class="top-slider">
				<div class="main-img">
					<a href="#"><img class="main-breaking" src="images/news/breaking.png"></a>
					<div class="breaking-title">
						<p>Lorem ipsum dolor sit amet, consectetur Nulla quis lorem neque, mattis venenatis lectus.</p>
					</div>
				</div>

				<div class="breaking-flair"><p>Breaking news</p></div>

				<div class="secondary-img">
						<div><a href="#"><img src="images/news/slider-img1.png"></a></div>
						<div><a href="#"><img src="images/news/slider-img2.png"></a></div>
						<div><a href="#"><img src="images/news/slider-img3.png"></a></div>
						<div><a href="#"><img src="images/news/slider-img4.png"></a></div>
				</div>
			</div>

		<div class="news-section-wrapper">
			<div class="article-blocks-main">

				<section class="news-block">
					<div class="section-title">
						<h2>From around the world</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>
					

						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>
						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>
						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>

					

				</section>

				<section class="news-block">
					<div class="section-title">
						<h2>Latest articles</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>

						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>
						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>
						<section>
							<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
							<a href=""><h4>Lorem ipsum dolor sit amet, consectetur </h4></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
							<a class="read-more" href="#">Read more</a>
						</section>

				</section>

				<section class="gallery">
					<div class="section-title">
						<h2>Gallery</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>
					

						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>
						<a href=""><img src="images/news/thumbnail-placeholder.png"></a>

				</section>
				<section class="tech-news">
					<div class="section-title">
						<h2>Tech news</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>
					

						<section>
							<a href=""><h3>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h3></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor...</p>
							<p>by <a href="">John Doe</a> &#124; 29 comments</p>
						</section>
						<section>
							<a href=""><h3>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h3></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor...</p>
							<p>by <a href="">John Doe</a> &#124; 29 comments</p>
						</section>
						<section>
							<a href=""><h3>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h3></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor...</p>
							<p>by <a href="">John Doe</a> &#124; 29 comments</p>
						</section>
						<section>
							<a href=""><h3>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h3></a>
							<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor...</p>
							<p>by <a href="">John Doe</a> &#124; 29 comments</p>
						</section>
				</section>
			</div>

			<div class="article-blocks-secondary">
				<section class="from-the-desk">

					<div class="section-title">
						<h2>From the desk</h2>
					</div>
					

					<section>
						<a href=""><h3>Lorem ipsum dolor sit amet, consectetur </h3></a>
						<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
						<a class="read-more" href="#">Read more</a><span>3 hours ago</span>
					</section>
					<section>
						<a href=""><h3>Lorem ipsum dolor sit amet, consectetur </h3></a>
						<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
						<a class="read-more" href="#">Read more</a><span>3 hours ago</span>
					</section>
					<section>
						<a href=""><h3>Lorem ipsum dolor sit amet, consectetur </h3></a>
						<p>Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.</p>
						<a class="read-more" href="#">Read more</a><span>3 hours ago</span>
					</section>

					
					<a href="">More &#43;</a>
				</section>

				<section class="editorial">
					<div class="section-title">
						<h2>Editorial</h2>
					</div>
					<section>
						<a href=""><img src="images/news/editorial-placeholder.png"></a>
						<a href=""><h4>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h4></a>
					</section>
					<section>
						<a href=""><img src="images/news/editorial-placeholder.png"></a>
						<a href=""><h4>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h4></a>
					</section>
					<section>
						<a href=""><img src="images/news/editorial-placeholder.png"></a>
						<a href=""><h4>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h4></a>
					</section>
					<section>
						<a href=""><img src="images/news/editorial-placeholder.png"></a>
						<a href=""><h4>Lorem ipsum dolor sit amet con se cte tur adipiscing elit</h4></a>
					</section>
				</section>
			</div>
			</div>
		</div>

		<div class="sidebar">
			<div class="video-bar">

				<section>
					<a href=""><img src="images/video-thumbnail-placeholder.png"></a>
					<a href=""><h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4></a>
				</section>
				<section>
					<a href=""><img src="images/video-thumbnail-placeholder.png"></a>
					<a href=""><h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4></a>
				</section>
				<section>
					<a href=""><img src="images/video-thumbnail-placeholder.png"></a>
					<a href=""><h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4></a>
				</section>
				<section>
					<a href=""><img src="images/video-thumbnail-placeholder.png"></a>
					<a href=""><h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4></a>
				</section>


				<a href="" class="more-plus">More &#43;</a>
					
			</div>
			<div class="sidebar-ad">
				<a href=""><img src="images/ad-sidebar.png"></a>
			</div>
			<div class="sign-up-form">
				<h2>Sign up for newsletter</h2>
				<p>Sign up to receive our free newsletters!</p>
				<form>
					<input type="text" name="name" placeholder="Name">
					<input type="text" name="email" placeholder="Email Address">
					<button>Submit</button>
				</form>
				<p>We do not spam. We value your privacy!</p>
			</div>
			<div class="popular-most-read">
				<div class="section-title">
					<a onclick="showTab(0)" class="tab-link"><h2 class="tabTitle">Popular</h2></a>
					<a onclick="showTab(1)" class="tab-link"><h2 class="tabTitle">Most read</h2></a>
				</div>
				
				<div class="tabContent" id="popular">
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					
					<a href="">More &#43;</a>
				</div>

				<div class="tabContent" id="most-read">

					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<section>
						<p>Sept 24th 2012</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>

					<a href="" class="more-plus">More &#43;</a>
				</div>
				
			</div>
			<div class="subscribe-banner">
				<a href="#"><img src="images/ad-subscribe.png"></a>
				<a href="#" class="subscribe-banner-btn">Subscribe now</a>
			</div>
		</div>
	</main>
	<div class="ad-bottom-banner">
		<a href="#"><img src="images/ad-bottom-banner.png"></a>
	</div>

	<footer>
		<ul class="horizontalmenu bottommenu">
			<li><a href="news-main.php?category=worldnews">World news</a></li>
			<li><a href="news-main.php?category=sports">Sports</a></li>
			<li><a href="news-main.php?category=tech">Tech</a></li>
			<li><a href="news-main.php?category=business">Business</a></li>
			<li><a href="news-main.php?category=movies">Movies</a></li>
			<li><a href="news-main.php?category=entertainment">Entertainment</a></li>
			<li><a href="news-main.php?category=culture">Culture</a></li>
			<li><a href="news-main.php?category=books">Books</a></li>
			<li><a href="news-main.php?category=classifieds">Classifieds</a></li>
			<li><a href="news-main.php?category=blogs">Blogs</a></li>
		</ul>

	<p>Copyright &copy; 2011 The News Reporter Inc. All rights reserved. Theme designed by GraphicsFuel.com</p>
	<p>Reproduction in whole or in part in any form or medium without express written permission of The News Reporter Inc. is prohibited. The trade marks and images used in the design are the copyright of their respective owners. They are used only for display purpose.</p>

	</footer>
</div>

<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>