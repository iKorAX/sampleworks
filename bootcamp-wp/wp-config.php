<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_bootcamp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K1#Rr[B?4kLtxPzzGVsTD>N4er4Q7j8dN{z%4KsoNj|Z0Cp}wJ>5l,sbb+fIjF!,');
define('SECURE_AUTH_KEY',  'uD`1F<Qc`3uF]]u#&,X!0}Y2+E@3eQ7|Ev&p/YOhE8s*jJ&J^v{S#CjR<i>#*1vh');
define('LOGGED_IN_KEY',    '=p1Lv-ti@jGbr$6E-)y@+ !8PCbPLmq$aXf~loMxs.5NN(2G#%i^Q_R:vqMs:$p|');
define('NONCE_KEY',        'D6AIcSU1l]t]+,jCANu37}aaMub2zV*NVtbhyq0{?q|;%)>NidqRg;stk1^kpzB]');
define('AUTH_SALT',        'CDV/;[sjL&dQXvnZAKyVf @dCO[j/gHa ZsQ|253hQ4ro=}thViGk7eQb+Ga*d=i');
define('SECURE_AUTH_SALT', 'y,Zaa&`7WE@+>lp|k7ZQ~Mi,n,m5~x4Pb9BHfaUY8.D+J-uWyZ}P#K1O|J[v5&(f');
define('LOGGED_IN_SALT',   'rL>]B5tGOUg=}H0RQ>]5p!&Ki.~lcsvNgj5nztB!qNEZq&{<ZVEeB;J+u_H3=AnD');
define('NONCE_SALT',       '6G]-Z-iLGmvz$d^[^,Uh8)oiIs?#TL`NYjYv|}?1[jBP)4<u|ez36~@#,Al`8a-|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
