<?php

// Įjungiame post thumbnail

add_theme_support( 'post-thumbnails' );
 add_image_size('thumbnail',150 , 150,true);
 add_image_size('large-thumbnail',300 , 300,true);


// Apsibrėžiame stiliaus failus ir skriptus

if( !defined('ASSETS_URL') ) {
	define('ASSETS_URL', get_bloginfo('template_url'));
}

function theme_scripts(){

    if ( !is_admin() ) {

        wp_deregister_script('jquery');
				wp_register_script('jquery', ASSETS_URL . '/assets/js/jquery-3.2.1.min.js', false, '3.2.1', true);

        wp_register_script('google-plus', 'https://apis.google.com/js/platform.js', false, '1.0', true);
        wp_enqueue_script('google-plus');

        wp_register_script('js_main', ASSETS_URL . '/assets/js/script.js', array('jquery'), '1.0', true);
        wp_enqueue_script('js_main');
    }
}
add_action('wp_enqueue_scripts', 'theme_scripts');


function theme_stylesheets(){

	$styles_path = ASSETS_URL . '/assets/css/styles.css';

	if( $styles_path ) {
	
		wp_register_style('ionicons', 'http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', false, false, false);
		wp_enqueue_style('ionicons');
		wp_register_style('bebas-regular', ASSETS_URL . '/assets/fonts/bebas_regular_macroman/stylesheet.css', false, false, false);
		wp_enqueue_style('bebas-regular');
		wp_register_style('styles', ASSETS_URL . '/assets/css/styles.css', array('ionicons','bebas-regular'), false, 'all');
		wp_enqueue_style('styles');
	}
}
add_action('wp_enqueue_scripts', 'theme_stylesheets');

// Apibrėžiame navigacijas

function register_theme_menus() {
   
	register_nav_menus(array( 
        'primary-navigation' => __( 'Primary Navigation' ),
        'logo-line-navigation' => __( 'Logo Line Navigation' )
    ));
}

add_action( 'init', 'register_theme_menus' );

// Apibrėžiame widgets juostas

$sidebars = array( 'Footer Widgets', 'Blog Widgets' );

if( isset($sidebars) && !empty($sidebars) ) {

	foreach( $sidebars as $sidebar ) {

		if( empty($sidebar) ) continue;

		$id = sanitize_title($sidebar);

		register_sidebar(array(
			'name' => $sidebar,
			'id' => $id,
			'description' => $sidebar,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));
	}
}

add_filter('widget_text', 'do_shortcode');

// Custom posts

$themePostTypes = array(
//'Testimonials' => 'Testimonial'

);

function createPostTypes() {

	global $themePostTypes;
 
	$defaultArgs = array(
		'taxonomies' => array('category'), // uncomment this line to enable custom post type categories
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		#'menu_icon' => '',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'has_archive' => true, // to enable archive page, disabled to avoid conflicts with page permalinks
		'menu_position' => null,
		'can_export' => true,
		'supports' => array( 'title', 'editor', 'thumbnail', /*'custom-fields', 'author', 'excerpt', 'comments'*/ ),
	);

	foreach( $themePostTypes as $postType => $postTypeSingular ) {

		$myArgs = $defaultArgs;
		$slug = 'vcs-starter' . '-' . sanitize_title( $postType );
		$labels = makePostTypeLabels( $postType, $postTypeSingular );
		$myArgs['labels'] = $labels;
		$myArgs['rewrite'] = array( 'slug' => $slug, 'with_front' => true );
		$functionName = 'postType' . $postType . 'Vars';

		if( function_exists($functionName) ) {
			
			$customVars = call_user_func($functionName);

			if( is_array($customVars) && !empty($customVars) ) {
				$myArgs = array_merge($myArgs, $customVars);
			}
		}

		register_post_type( $postType, $myArgs );

	}
}

if( isset( $themePostTypes ) && !empty( $themePostTypes ) && is_array( $themePostTypes ) ) {
	add_action('init', 'createPostTypes', 0 );
}


function makePostTypeLabels( $name, $nameSingular ) {

	return array(
		'name' => _x($name, 'post type general name'),
		'singular_name' => _x($nameSingular, 'post type singular name'),
		'add_new' => _x('Add New', 'Example item'),
		'add_new_item' => __('Add New '.$nameSingular),
		'edit_item' => __('Edit '.$nameSingular),
		'new_item' => __('New '.$nameSingular),
		'view_item' => __('View '.$nameSingular),
		'search_items' => __('Search '.$name),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Bin'),
		'parent_item_colon' => ''
	);
}


//ACF options pages
if( function_exists('acf_add_options_page')){
	acf_add_options_page("Global");
}

//Custom menu function

function category_menu($menu_class, $location){
	wp_nav_menu(array(
		'container' => 'ul',
		'menu_class' => $menu_class,
		'theme_location' => $location,
		'menu' => $location
	));
}



function news_blocks_index($categoryName){
	$category_id = get_cat_ID($categoryName);
	$category_link = get_category_link( $category_id );
	return $category_link;
}


function get_YT_thumbnail($video_id){
	$thumbLink ='https://img.youtube.com/vi/' . $video_id . '/0.jpg';
	return $thumbLink;
}

function get_YT_link($video_id){
	$videoLink = 'https://www.youtube.com/watch?v=' . $video_id;
	return $videoLink;
}


function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

if(function_exists('dynamic_sidebar') && dynamic_sidebar('Blog Widgets'));

?>

