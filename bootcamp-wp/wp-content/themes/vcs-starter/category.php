<?php 

	get_header();

	$category = get_category($cat);
	$count = $category->category_count;

?>

<main>
	<div class="newsdiv">
	<section class="news-category">	
		<div class="section-title">
			<h2><?php echo get_cat_name( $cat); ?></h2>

			<?php 

			$args = array(
			  'cat' => $cat,
			);
			$the_query = new WP_Query( $args );

			if( $the_query->found_posts ): ?>
			</div>
			<?php query_posts('posts_per_page=20'); ?>
			<?php 
				while ( $the_query->have_posts() ):	
					$the_query->the_post(); 
					$url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
					?>
			<section class="single-report">
				<a href="<?php echo wp_get_shortlink(); ?>">
					<img src="<?php echo the_post_thumbnail_url('thumbnail'); ?>">
				<div>
					<a href=""><h4 class="report-title"><?php the_title(); ?></h4></a>
					<p><?php the_excerpt(); ?></p>
					<a class="read-more" href="<?php echo wp_get_shortlink(); ?>">Read more</a>
				</div>
			</section>
			<?php endwhile; ?>
			
			<?php else: ?>

	    <h2><?php echo 'no posts found'; ?></h2>
	    </div>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
</section>
</div>

<?php get_sidebar(); ?>

</main>

<?php get_footer(); ?>