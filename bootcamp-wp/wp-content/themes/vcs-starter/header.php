<!DOCTYPE html>

<html lang="<?php bloginfo('language'); ?>">
<meta charset="<?php bloginfo('charset'); ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>
</head>

<body>
	<div class="body-container">
	<?php get_template_part('elements/site-header'); ?>
