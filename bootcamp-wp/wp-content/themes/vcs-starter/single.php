<?php 

	get_header();

	$category = get_category($cat);
	$count = $category->category_count;

	if( !get_post_meta($post -> ID,'views')){
		add_post_meta($post->ID,'views',1);
	} else{
		$views = get_post_meta($post->ID,'views');
		$final = $views[0] + 1;
		update_post_meta($post->ID,'views',$final,$views[0]);
	}
?>
<main>
	<div class="newsdiv">
		<?php the_post(); ?>
		<section class="report-wrapper">
					<div class="report-picture">
						<img src="<?php echo the_post_thumbnail_url(); ?>">
					</div>

					<div class="section-title">
						<h2><?php the_title(); ?></h2>
					</div>

					
					<div class="report-text">
						<p><?php the_content(); ?></p>
					</div>
				</section>
	</div>
<?php get_sidebar(); ?>

</main>

<?php get_footer(); ?>