<div class="sidebar">
			<div class="video-bar">

<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'category_name' => 'video'
	);
	$query = new WP_Query( $args );


if($query->have_posts()):
	$i = 0;
	while($query->have_posts() && $i < 4): $query->the_post();
 	?>

				<section>
					<a target="_blank" href="<?php echo get_YT_link(get_the_content()); ?>"><img src="<?php echo get_YT_thumbnail(get_the_content()); ?>"></a>
					<a target="_blank" href="<?php echo get_YT_link(get_the_content()); ?>"><h4><?php echo the_title(); ?></h4></a>
				</section>

<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $query, $args);
 ?>

<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'meta_value_num',
	
	'meta_type' => 'NUMERIC',
	'meta_key' => 'post_views_count',
	'post_status' => 'publish',
	'order' => 'DESC',
	'cat' => '-14'
	);
	$query = new WP_Query( $args );


if($query->have_posts()):
	$i = 0;
	while($query->have_posts() && $i < 4): $query->the_post();
 	?>

				<section>
					<a target="_blank" href="<?php echo get_YT_link(get_the_content()); ?>"><img src="<?php echo get_YT_thumbnail(get_the_content()); ?>"></a>
					<a target="_blank" href="<?php echo get_YT_link(get_the_content()); ?>"><h4><?php echo the_title(); ?></h4></a>
				</section>

<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $query, $args);
 ?>

				<a href="" class="more-plus">More &#43;</a>
					
			</div>
			<div class="sidebar-ad">
				<a href=""><img src="<?php echo ASSETS_URL . '/assets/images/ad-sidebar.png'; ?>"></a>
			</div>
			<div class="sign-up-form">
				<h2>Sign up for newsletter</h2>
				<p>Sign up to receive our free newsletters!</p>
				<form>
					<input type="text" name="name" placeholder="Name">
					<input type="text" name="email" placeholder="Email Address">
					<button>Submit</button>
				</form>
				<p>We do not spam. We value your privacy!</p>
			</div>
			<div class="popular-most-read">
				<div class="section-title">
					<a onclick="showTab(0)" class="tab-link"><h2 class="tabTitle">Popular</h2></a>
					<a onclick="showTab(1)" class="tab-link"><h2 class="tabTitle">Most read</h2></a>
				</div>
				
				<div class="tabContent" id="popular">
					<?php for($i = 0; $i < 5; $i++): ?>
					<section>
						<p>Sept 24th 2011</p>
						<h4>Lorem ipsum dolor sit amet conse ctetur adipiscing elit</h4>
						<a class="read-more" href="#">Read more</a>
					</section>
					<?php endfor; ?>
					
					<a href="">More &#43;</a>
				</div>


				<div class="tabContent" id="most-read">
					

<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'meta_value',
	'meta_key' => 'views',
	'order' => 'DESC',
	'cat' => '-14'
	);

	$query = new WP_Query( $args );
 if( $query->have_posts() ):
	$i = 0;
	while($query->have_posts() && $i < 5): $query->the_post(); ?>
		<section>
			<p><?php echo get_the_date(); ?></p>
			<h4><?php the_title(); ?></h4>
			<a class="read-more" href="<?php echo wp_get_shortlink(); ?>">Read more</a>
		</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $query);
 ?>
					<a href="">More &#43;</a>
				</div>
				
			</div>
			<div class="subscribe-banner">
				<a href=""><img src="<?php echo ASSETS_URL . '/assets/images/ad-subscribe.png'; ?>"></a>
				<a href="" class="subscribe-banner-btn">Subscribe now</a>
			</div>
</div>