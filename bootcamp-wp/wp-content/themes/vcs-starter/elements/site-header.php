<header class="page-header">
	<div class="logo-line">

		<div class="logo-menu">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo ASSETS_URL . '/assets/images/logo.png' ?>" class="logo"></a>
		
				<div class="burger">
					<div></div>
					<div></div>
					<div></div>
				</div>
		
				<?php category_menu('header-menu','logo-line-navigation'); ?>
		</div>

		<div id="dark-cover">
			<div class="inpage-window" id="about-window">
					<h2>A brief history of everything</h2>
					<p>The News Reporter is a British public service broadcaster with its headquarters at Broadcasting House in London. The News Reporter is the world's oldest national broadcasting organisation and the largest broadcaster in the world by number of employees. It employs over 20,950 staff in total, 16,672 of whom are in public sector broadcasting. The total number of staff is 35,402 when part-time, flexible, and fixed contract staff are included. <br> <br>

					The News Reporter is established under a Royal Charter and operates under its Agreement with the Secretary of State for Culture, Media and Sport. Its work is funded principally by an annual television licence fee which is charged to all British households, companies, and organisations using any type of equipment to receive or record live television broadcasts and iPlayer catch-up. The fee is set by the British Government, agreed by Parliament, and used to fund the News Reporter's radio, TV, and online services covering the nations and regions of the UK. Since 1 April 2014, it has also funded The News Reporter World Service (launched in 1932 as the BBC Empire Service), which broadcasts in 28 languages and provides comprehensive TV, radio, and online services in Arabic and Persian.
					</p>
			</div>
			<div class="inpage-window" id="contact-window">
				<form>
					<h2>Send us a message</h2>
					<input type="text" name="email" placeholder="Your name">
					<input type="" name="" placeholder="Your e-mail">
					<textarea placeholder="Your message..."></textarea>
					<button>Send</button>
				</form>
			</div>

			<div class="inpage-window" id="subscribe-window">
				<form>
					<h2>Subscribe to our daily digest</h2>
					<input type="text" name="email" placeholder="Your e-mail address">
					<input type="text" name="user_name" placeholder="Your name">
					<p>By clicking submit you are agreeing to the Terms and Conditions.</p>
					<button>Subscribe</button>
				</form>
			</div>
			<div class="inpage-window" id="login-window">
				<form>
					<h2>Log in to add content</h2>
					<input type="text" name="email" placeholder="Your username">
					<input type="password" name="user_name" placeholder="Your password">
					<button>Log in</button>
				</form>
			</div>
		</div>

		<div class="social-and-search">
			<div class="social">
				<a href="" class="twitter-link"><i class="icon ion-social-twitter"></i></a>
				<a href="" class="fb-link"><i class="icon ion-social-facebook"></i></a>
				<a href="" class="rss-link"><i class="icon ion-social-rss"></i></a>
				<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=100&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId" width="80" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				<div class="google-button"><div class="g-plusone" data-size="tall" ... ></div></div>
			</div>

			<div class="search">
				<form class="search-form">
					<input type="text" class="searchbar" name="search">
					<button>
						<i class="icon ion-search"></i>
					</button>
				</form>
			</div>
		</div>
	</div>

	<div class="newsmenu-wrapper">
	<a class="show-categories" href="#">Categories <span>&#9656;</span></a>
		<?php category_menu('horizontalmenu topmenu','primary-navigation'); ?>

	</div>
</header>