<main>
		<div class="newsdiv">
			<div class="top-slider">
				<div class="main-img">
<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'tag' => 'breaking'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1; 
	while($query->have_posts() && $i < 5): $query->the_post();
?>
					<a class="mySlides" href="<?php echo wp_get_shortlink(); ?>"><img class="main-breaking" src="<?php echo the_post_thumbnail_url(); ?>"></a>
					<div class="breaking-title">
						<p><?php echo excerpt(12); ?></p>
					</div>
<?php 
	endwhile;
	$i = 1;

?>


				</div>

				<div class="breaking-flair"><p>Breaking news</p></div>

				<div class="secondary-img">
<?php 
	while($query->have_posts() && $i < 5): $query->the_post();
 	?>
						<div><a href="#" onclick="currentDiv(<?php echo $i; ?>)"><img class="demo active-slider-img" src="<?php echo the_post_thumbnail_url('thumbnail'); ?>">'; ?>>
						</a></div>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>

 <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var slideImg = document.getElementsByClassName("mySlides");
  var slideTitle = document.getElementsByClassName("breaking-title");
  var dots = document.getElementsByClassName("demo");
  if (n > slideImg.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slideImg.length}
  for (i = 0; i < slideImg.length; i++) {
     slideImg[i].style.display = "none";
     slideTitle[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" active-slider-img", "");
  }
  slideImg[slideIndex-1].style.display = "block";
  slideTitle[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active-slider-img";
}
</script>
				</div>
			</div>

		<div class="news-section-wrapper">
			<div class="article-blocks-main">

				<section class="news-block">
					<div class="section-title">
						<h2>From around the world</h2>
						<a href="<?php echo news_blocks_index('World News'); ?>" class="more-plus">More &#43;</a>
					</div>
<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'category_name' => 'world-news'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1;
	while($query->have_posts() && $i < 4): $query->the_post();
 	?>
						<section>
							<a class="thumb-link" href="<?php echo wp_get_shortlink(); ?>">
								<img src="<?php echo the_post_thumbnail_url('large-thumbnail'); ?>">
							</a>
							<a class="" href="<?php echo wp_get_shortlink(); ?>"><h4><?php the_title(); ?></h4></a>
							<p><?php echo excerpt(12); ?></p>
							<a class="read-more" href="<?php echo wp_get_shortlink(); ?>">Read more</a>
						</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>

				</section>

				<section class="news-block">
					<div class="section-title">
						<h2>Latest articles</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>
<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'cat' => '-14'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1;
	while($query->have_posts() && $i < 4): $query->the_post();
 	?>
						<section>
							<a class="thumb-link" href="<?php echo wp_get_shortlink(); ?>">
								<img src="<?php echo the_post_thumbnail_url('large-thumbnail'); ?>">
							</a>
							<a href="<?php echo wp_get_shortlink(); ?>"><h4><?php the_title(); ?></h4></a>
							<p><?php echo excerpt(12); ?></p>
							<a class="read-more" href="<?php echo wp_get_shortlink(); ?>">Read more</a>
						</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>
				</section>

				<section class="gallery">
					<div class="section-title">
						<h2>Gallery</h2>
						<a href="" class="more-plus">More &#43;</a>
					</div>
<?php
	$args = array(
	'post_type' => 'post',
	'meta_key' => '_thumbnail_id',
	'orderby' => 'rand',
	'cat' => '-14'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1;
	while($query->have_posts() && $i < 7): $query->the_post();
 	?>
					
						<a href="<?php echo wp_get_shortlink(); ?>"><img src="<?php echo the_post_thumbnail_url('large-thumbnail'); ?>"></a>
					
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>
				</section>



				<section class="tech-news">
					<div class="section-title">
						<h2>Tech news</h2>
						<a href="<?php echo news_blocks_index('Tech'); ?>" class="more-plus">More &#43;</a>
					</div>
<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'category_name' => 'tech'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1;
	while($query->have_posts() && $i < 5): $query->the_post();
 	?>
						<section>
							<a href="<?php echo wp_get_shortlink(); ?>"><h3><?php the_title(); ?></h3></a>
							<p><?php echo excerpt(12); ?></p>
							<p>by <a href="">John Doe</a> &#124; 29 comments</p>
						</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>
				</section>
			</div>

			<div class="article-blocks-secondary">
				<section class="from-the-desk">

					<div class="section-title">
						<h2>From the desk</h2>
					</div>
					

<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'tag' => 'from-the-desk'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1; 
	while($query->have_posts() && $i < 5): $query->the_post();
?>
					<section>
						<a href=""><h3><?php the_title(); ?></h3></a>
						<p><?php echo excerpt(12); ?></p>
						<a class="read-more" href="<?php echo wp_get_shortlink(); ?>">Read more</a><span><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
					</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>
					<a href="">More &#43;</a>
				</section>

				<section class="editorial">
					<div class="section-title">
						<h2>Editorial</h2>
					</div>

<?php
	$args = array(
	'post_type' => 'post',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'tag' => 'editorial'
	);
	$query = new WP_Query( $args );
if($query->have_posts()):
	$i = 1; 
	while($query->have_posts() && $i < 5): $query->the_post();
?>
					<section>
						<a class="thumb-link" href="<?php echo wp_get_shortlink(); ?>"><img src="<?php echo the_post_thumbnail_url('large-thumbnail'); ?>"></a>
						<a href="<?php echo wp_get_shortlink(); ?>"><h4><?php the_title(); ?></h4></a>
					</section>
<?php 
	$i++;
	endwhile;
	endif;
	unset($i, $args, $query);
 ?>

				</section>
			</div>
			</div>
		</div>

