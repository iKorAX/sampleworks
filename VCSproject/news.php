<!DOCTYPE html>
<html>

<head>
	<title>News</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

</head>
<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "mybase";

$jungtis = new mysqli($servername, $username, $password, $database);
?>

<body class="bgimg">

	<?php include 'header.php' ?>

	<!-- START of markup for breadcrumbs here
	I need to change the color of this and make it change when the page we're at changes
	 -->
	<nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="news.php" class="breadcrumb">News</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->

  <center><h3>News</h3></center>

  	<!-- START of news cards -->

		<div class="row">
	  		<div class="col s12 m12 l10 offset-l1 newsdiv">
	  		<?php 
	  			$sql = "SELECT * FROM naujienos ORDER BY id DESC";
      			$rezultatas = $jungtis->query($sql);

      			while($irasas = $rezultatas->fetch_assoc()): ?>	

		  		<div class="row">
					<div class="card horizontal">
						<div id="foto" class="card-image col s3 z-depth-4">
						<img id="fotos" src=" <?php echo $irasas['Image']; ?>"/>
						</div>
						<div class="card-stacked">
							<div>
								<div class="row">
									<div class="col s10">
										<h2 id="newspav"><?php echo $irasas['Title']; ?></h2>
          								<p><?php echo $irasas['Summary']; ?>
          									<a class="waves-effect waves-light blue btn hide-on-med-and-up" onclick="location.href='newsarticle.php?id=<?php echo $irasas["id"]; ?>'">Read more</a>
          								</p>
									</div>
								
									<div id="btn" class="col s2 cardbutton">

										<button id="rock" class="custombuttons z-depth-4 hide-on-small-only" type="button" name="pirmasMygtukas" onclick="location.href='newsarticle.php?id=<?php echo $irasas["id"]; ?>'">Read more</button>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			<?php 

            endwhile;
            ?>


			
			</div>
		</div>

<?php include 'newsmodal.php' ?>


<div class="fixed-action-btn horizontal click-to-toggle">
    <a id="addstory" class="btn-floating btn-large blue modal-trigger waves-effect waves-light" href="#modal1">
      <i class="material-icons">add</i>
    </a>
  </div>

	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>

</html>