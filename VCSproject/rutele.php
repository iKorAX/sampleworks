<!DOCTYPE html>
<html>

<head>
	<title></title>
	<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
  <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet"> 
  <!--reset black screen for matrix background -->
  <style> 
     *{
        margin: 0;
        padding: 0;
      }     
      .orange-goblin {
        border: 5px solid #ffab00;
      }
      body {background: black;}
      canvas {display:block;}  
  </style>
  <!--graphs -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
  <!-- for mobile -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
  <!--top navigation bar START -->
      <!-- About Us dropdown markup -->
  <?php include 'header.php' ?>
  <!--top navigation bar END --> 


  <!-- START of breadcrumbs 
  I need to change the color of this and make it change when the page we're at changes
   -->
  <nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="about_us.php" class="breadcrumb">About us</a>
        <a href="rutele.php" class="breadcrumb">Rutele</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->



  <!-- matrix in the background, oh yeah! >:) -->
  <canvas id="c" style="position: absolute">
  </canvas>   
  <script>
    //getting canvas by id c
        var c = document.getElementById("c");
        var ctx = c.getContext("2d");
        var font_size = 10;

    //making the canvas full screen
        c.height = window.innerHeight;
        c.width = window.innerWidth;

    //characters to the charset
        var matrix = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789@#$%^&*()*&^%";
    //convert string to array of single characters
        matrix = matrix.split("");

    //number of columns for the rain
        var columns = c.width/font_size; 
    //an array of drops - one per column
        var drops = [];
    //x below is the x coordinate
    //1 = y co-ordinate of the drop(same for every drop initially)
        for(var x = 0; x < columns; x++)
            drops[x] = 1; 

    //drawing the characters
        function draw()
        {
            //Black BG for the canvas
            //translucent BG to show trail
            ctx.fillStyle = "rgba(0, 0, 0, 0.04)";
            ctx.fillRect(0, 0, c.width, c.height);
            //text colour = amber
            ctx.fillStyle = "#ffab00"; 
            ctx.font = font_size + "px arial";
            //looping over drops
            for(var i = 0; i < drops.length; i++)
            {
                //a random character to print
                var text = matrix[Math.floor(Math.random()*matrix.length)];
                //x = i*font_size, y = value of drops[i]*font_size
                ctx.fillText(text, i*font_size, drops[i]*font_size);

                //sending the drop back to the top randomly after it has crossed the screen
                //adding a randomness to the reset to make the drops scattered on the Y axis
                if(drops[i]*font_size > c.height && Math.random() > 0.975)
                    drops[i] = 0;
                //incrementing Y coordinate
                drops[i]++;
            }
        }
        setInterval(draw, 35);
    </script>
  <!-- background is awesome, you may continue -->


<!-- TITLE CARD -->
  <div class="row ">
    <div class="col s9 offset-s1">
      <div class="card horizontal orange-goblin" >
        <div class="card-stacked">
          <div class="card-content">
            <h5>Rutele Marciulionyte</h5>
            <p>is looking for a job now! :)</p>
          </div>
          <div class="card-action">
            <a href="https://www.linkedin.com/in/rutele-m" class="col s12 waves-effect waves-light btn #ffd54f amber lighten-1">linked in profile</a>
          </div>
        </div>
        <div class="card-image">
              <img src="img/rutele/profile.jpg"/>
        </div>
      </div>
    </div>
  </div>

  <div class="row ">
    <div class="col s9 offset-s1">
      <div class="card horizontal orange-goblin">
        <div class="card-stacked">
          <div class="card-content">
            <p class="center-align">
            apart from this, please feel free to browse this page further...</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">


<!-- 1. MR. ROBOT CARD -->
    <div class="col s4">
      <div class="card orange-goblin">
        <div class="card-image center-align">
              <img class="indexbottompics" src="img/rutele/hackerman.jpg">
        </div>
        <div class="card-content">
            <p class="flow-text center-align" style="font-family: 'Righteous', cursive, center; ">When ur internet isn't working and you reset the router to fix it</p>
        </div>
      </div>
    </div>


<!-- 2. CAT CARD -->
    <div class="col s4">
      <div class="card orange-goblin">
        <div class="card-image center-align">
            <img class="indexbottompics" src="https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif"> 
        </div>
        <div class="card-content">
            <p class="flow-text center-align " style="font-family: 'Righteous', cursive, center; ">When ur desperately tryin to fix a bug 15 min till the deadline</p>
        </div>
      </div>
    </div>   


<!-- 3. GRAPH CARD -->
    <div class="col s4">
      <div class="card orange-goblin">
       
        <canvas id="myChart" width="400" height="400"></canvas>
        <script>
        var cty = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(cty, {
            type: 'bar',
            data: {
                labels: ["TRYS", "Saunuoliai", "x"],
                datasets: [{
                    label: '# of git commits',
                    data: [9051, 100, 150],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script>
        <div class="card-content">
            <p class="flow-text center-align " style="font-family: 'Righteous', cursive, center; ">When ur team commits changes every 2 min (over 9000!)</p>
        </div>
      </div>
    </div>
  </div>
  
	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<!-- animate css - to add animations,     wow.js    -->