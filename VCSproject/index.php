<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "mybase";

$jungtis = new mysqli($servername, $username, $password, $database);
?>

<html>

<head>
	<title></title>

	<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body class="bgimg">
	<?php include 'header.php' ?>


	<!-- START of markup for breadcrumbs here
	I need to change the color of this and make it change when the page we're at changes
	 -->
	<nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="#!" class="breadcrumb">Home</a>

      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->
<?php include 'newsmodal.php' ?>
  	<!-- START of the slider with the big picture on the index page -->
    <div class="row">
	<div class="slider col s12">
	    <ul class="slides">
	      <li>
	        <img src="img/index/1.png">
	        <div class="caption center-align">
	          <h3>Travel with us!</h3>
	          <h5 class="light grey-text text-lighten-3">Would you like to read some travel news?</h5>
	          <a class="waves-effect btn blue lighten-4 sliderbuttons" href="news.php">Yes!</a>
	        </div>
	      </li>
	      <li>
	        <img src="img/index/2.jpg"> 
	        <div class="caption center-align">
	          <h3 class="black-text" style="text-shadow: 1px 1px 2px white;" >Make your own news!</h3>
	          <h5 class="black-text" style="text-shadow: 1px 1px 2px white;">Would you like to add something of your own?</h5>
	          <a id="addstory" class="btn blue modal-trigger sliderbuttons lighten-4  waves-effect waves-light" href="#modal1">Gladly!</a>
	        </div>
	      </li>
	      <li>
	        <img src="img/index/aboutus.jpg"> 
	        <div class="caption center-align">
	          <h3>Our team</h3>
	          <h5 class="light grey-text text-lighten-3">All about the cool cats</h5>
	          <a class="waves-effect waves-light sliderbuttons blue lighten-4 btn" href="about_us.php">Find out more!</a>
	        </div>
	      </li>
	    </ul>
	  </div>
	  </div>
	  <!-- END of the slider with pictures -->

	<!-- START of pictures with text at the bottom of index page -->

		<?php 
            $rezultatas = $jungtis->query("SELECT * FROM naujienos WHERE id=3");
            $row = $rezultatas->fetch_assoc();
            $pav = $row["Title"];
            $image = $row["Image"];
            $txt = $row["Summary"];
            $article = $row["Article"];
            $image2 = $row["Image2"];
            $image3 = $row["Image3"];
        ?>


	<div class="row">
            <?php 
            $sql = "SELECT * FROM naujienos order by rand() limit 3";
            $rezultatas = $jungtis->query($sql);
            while($irasas = $rezultatas->fetch_assoc()):
              for ($i=1;$i<2;$i++): ?>
                
                  <div class="col s12 m4">
                    <div class="card hoverable">
                      <div class="card-image center-align">
                        <img class="indexfotos" src="<?php echo $irasas["Image"] ?>"/>
                      </div>
                        <div class="card-content center-align">
                          <p class="articletitle cardnewstitle center-align"><?php echo $irasas["Title"]; ?></p>
                        </div>
                        <div class="card-action readmorelink">
                          <a class="" href="newsarticle.php?id=<?php echo $irasas["id"];?>">Read More</a>
                        </div>
                    </div>
                </div>
              
                <?php 
              endfor;
            endwhile;
            ?>
    <!-- END of pictures with text at the bottom of index page -->

	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

</body>
</html>
