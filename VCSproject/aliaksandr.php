<!DOCTYPE html>
<html>

<head>
	<title></title>
		<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/aliaksandr.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body class="bgimg">

	<!--top navigation bar START -->
<?php include 'header.php' ?>
	<!--top navigation bar END --> 


	<!-- START of markup for breadcrumbs here
	I need to change the color of this and make it change when the page we're at changes
	 -->
	<nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="about_us.php" class="breadcrumb">About Us</a>
        <a href="aliaksandr.php" class="breadcrumb">Aliaksandr</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->
  <img src="img/aliaksandr/maxresdefault.jpg" id="catpic">
<center><div class="row">
        <div class="col s12 m10">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Aliaksandr</span>
              <canvas id="myCanvas"></canvas>

<p align="center" style="font-size: 30px">What the darn-diddily-doodily did you just say about me, you little witcharooney? I’ll have you know I graduated top of my class at Springfield Bible College, and I’ve been involved in numerous secret mission trips in Capital City, and I have over 300 confirmed baptisms. I am trained in the Old Testament and I’m the top converter in the entire church mission group. You are nothing to me but just another heathen. I will cast your sins out with precision the likes of which has never been seen before in Heaven, mark my diddily-iddilly words. You think you can get away with saying that blasphemy to me over the Internet? Think again, friendarino. As we speak I am contacting my secret network of evangelists across Springfield and your IP is being traced by God right now so you better prepare for the storm, maggorino. The storm that wipes out the diddily little thing you call your life of sin. You’re going to Church, kiddily-widdily. Jesus can be anywhere, anytime, and he can turn you to the Gospel in over infinity ways, and that’s just with his bare hands. Not only am I extensively trained in preaching to nonbelievers, but I have access to the entire dang- diddily Bible collection of the Springfield Bible College and I will use it to its full extent to wipe your sins away off the face of the continent, you diddily-doo satan-worshipper. If only you could have known what holy retribution your little “clever” comment was about to bring down upon you from the Heavens, maybe you would have held your darn-diddily-fundgearoo tongue. But you couldn’t, you didn’t, and now you’re clean of all your sins, you widdillo-skiddily neighborino. I will sing hymns of praise all over you and you will drown in the love of Christ. You’re farn-foodily- flank-fiddily reborn, kiddo-diddily.</p>
            </div>
            <div class="card-action">
            </div>
          </div>
        </div>
      </div>
<center>

  <!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/aliaksandr.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

</body>

</html>