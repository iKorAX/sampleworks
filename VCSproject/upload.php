<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "mybase";

$jungtis = new mysqli($servername, $username, $password, $database);

$target_dir = "img/news/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        $name = $_FILES["fileToUpload"]["name"];
        $sql = "INSERT INTO naujienos (Title,Summary,Article,Image) VALUES ('".$_POST['Title']."','".$_POST['Summary']."','".$_POST['Article']."','img/news/".$name."');";
        $rezultatas = $jungtis->query($sql);
       
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
echo "You'll be redirected in 5 seconds.";
?>

<script type="text/javascript">
    window.setTimeout(redirectToNews, 5000);
    function redirectToNews() {
    window.location.href = "news.php";        
    }

</script>