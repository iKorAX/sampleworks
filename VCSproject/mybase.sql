-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2017 at 10:43 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybase`
--

-- --------------------------------------------------------

--
-- Table structure for table `naujienos`
--

CREATE TABLE `naujienos` (
  `id` int(11) NOT NULL,
  `Title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Article` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `naujienos`
--

INSERT INTO `naujienos` (`id`, `Title`, `Image`, `Article`, `Summary`, `Image2`, `Image3`) VALUES
(1, 'The 48 countries where a terrorist attack is most likely', 'img/news/paris.jpg', ' &nbsp&nbsp&nbsp Following the Manchester bombing MI5 has raised the UK\'s terror threat level to \"critical\", the third time it has reached the top rating since its inception. <br/>\r\n  &nbsp&nbsp&nbsp Meanwhile, as the map below shows, there are now more than 45 countries around the world where the Foreign Office believes a terror attack is \"very likely\" to be attempted. <br/> \r\n  &nbsp&nbsp&nbsp They include Germany, whose capital witnessed a deadly attack at a Christmas market in December, as well as France, Turkey and Belgium. The Paris terror attacks in November 2015 saw 130 killed, while in July last year 84 people died when a truck was driven through a crowd of people on the promenade in Nice. <br/>\r\n  &nbsp&nbsp&nbsp Turkey witnessed a string of attacks last year, most notably at Istanbul\'s Ataturk Airport, but also in the capital, Ankara, and the city of Gaziantep. The Brussels attacks in March 2016, meanwhile, saw 32 people killed.<br/>\r\n  &nbsp&nbsp&nbsp The top terror threat level is also given to Egypt, where a Russian aircraft was bombed in 2015, Tunisia, where 30 Britons were killed in a massacre during the same year, Indonesia, Russia, Myanmar, Kenya, the Philippines, Colombia, Thailand and Australia. ', 'Following the Manchester bombing MI5 has raised the UK\'s terror threat level to \"critical\", the third time it has reached the top rating since its inception.', 'img/news/paris2.jpg', 'img/news/paris3.jpg'),
(2, 'The travel checklist: Must-read books and must-watch movies', 'img/news/vocation.jpg', '  Their choices range from globally renowned classics -- like Austria\'s choice of Carol Reed\'s Vienna-set film \"The Third Man\" -- to quirky new releases, such as Malta\'s \"Limestone Cowboy,\" about a family dealing with a delusional father\'s ambition to become Prime Minister.<br/>\r\n  \"One theme we noticed is a focus on nature,\" says Julie Hansen, Babbel\'s newly appointed US CEO. Wildlife documentary \"Colombia, magia salvaje\" (2015), for example, showcases the country\'s glorious landscapes for an international audience more used to seeing them in the context of crime series like Netflix\'s Narcos.<br/>\r\n  \"Another common thread amongst the recommendations was love,\" adds Hansen, \"a theme that has inspired stories in almost every human culture in history.\"\r\nWhen immersing ourselves in a new culture, a nation\'s fictional works the stories it tells about itself offer insights as valuable as those we can receive from its non-fiction.<br/>\r\n\"Non-fiction [works] well in depicting the history that shaped a country and the can\'t-miss experiences that a visitor should see,\" says Hansen.<br/>\r\n  \"Fiction works wonderfully to boil down the experiences, beliefs attitudes, and characteristics of a country and its people. It is often better than non-fiction at capturing a zeitgeist.\"\r\nThe full list of recommendations is below and you can read more about the project here.', 'Their choices range from globally renowned classics -- like Austria\'s choice of Carol Reed\'s Vienna-set film \"The Third Man\" -- to quirky new releases, such as Malta\'s \"Limestone Cowboy,\" about a family dealing with a delusional father\'s ambition to become Prime Minister.', 'img/news/vocation2.jpg', 'img/news/vocation3.jpg'),
(3, 'Design your own multi-million dollar yacht', 'img/news/yacht.jpg', '  Dynamiq, a yachting company from Monaco, is now offering the uber-rich the ability to design and customize their own yachts.<br/>\r\n  Starting at just $13.3 million, those with cash to spare can now customize the color of the hull, the fabric of the seating and even the type of railing on their yacht all online.<br/>\r\n  Dynamiq offers two different styles of yacht to customize. The 115-foot GTT 115 has enough space for six guests and can reach speeds up to 21 knots. (Dynamiq will only produce seven of the fastest model.) The 130-foot GTT 130 is ideal for the Mediterranean, Caribbean or Asian islands and has enough room for five guest cabins.<br/>\r\nDynamiq offers two different styles of yacht to customize. The 115-foot GTT 115 has enough space for six guests and can reach speeds up to 21 knots. (Dynamiq will only produce seven of the fastest model.) The 130-foot GTT 130 is ideal for the Mediterranean, Caribbean or Asian islands and has enough room for five guest cabins.<br/>\r\n\r\nAfter logging onto the Dynamiq website, shoppers go through and customize every option of their yacht, down to the color of the electronics. A handy price option lets customers know exactly how much they are spending with each decision.\r\n', 'Dynamiq, a yachting company from Monaco, is now offering the uber-rich the ability to design and customize their own yachts.', 'img/news/yacht2.jpg', 'img/news/yacht3.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `naujienos`
--
ALTER TABLE `naujienos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `naujienos`
--
ALTER TABLE `naujienos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
