<?php echo '

		 <!-- Modal Structure -->
	<div id="modal1" class="modal modal-fixed-footer">
	<div class="modal-content">
	<h4>Add a news story</h4>


		<form id="newsform" class="col s12 my-form" accept-charset="UTF-8" action="upload.php" method="post" enctype="multipart/form-data">
		
		<div class="row">
			<div class="input-field col s12">
				<textarea id="Title" name="Title" class="materialize-textarea validate" required></textarea>
				<label for="Title">Title</label>
			</div>
		</div>


		
		<div class="row">
			<div class="input-field col s12">
				<textarea id="Summary" name="Summary" class="materialize-textarea" required></textarea>
				<label for="Summary">Summary</label>
			</div>
		</div>

		<div class="row">
			<div class="row">
				<div class="input-field col s12">
				<textarea id="Article" name="Article" class="materialize-textarea" required></textarea>
				<label for="Article">Article</label>
				</div>
			</div>
		</div>
			<div class="file-field input-field">
				<div class="blue btn">
					<span>Choose an image</span>
					<input class="blue" type="file" name="fileToUpload" required>
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text">
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type="submit" name="submit" class="btn blue">
		</div>
		</form>
	</div>

  ';
  ?>