<!DOCTYPE html>
<html>

<head>
	<title>Monica's page</title>
  <!-- amCharts javascript sources -->
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
    

    <!-- amCharts javascript code -->
    <script type="text/javascript">
      AmCharts.makeChart("chartdiv1",
          {
          "type": "pie",
          "angle": 12,
          "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
          "depth3D": 15,
          "titleField": "category",
          "valueField": "column-1",
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "align": "center",
            "markerType": "circle"
          },
          "titles": [
            {
              "size": 16,
              "text": "Countries i've visited so far"
            }
          ],
          "dataProvider": [
            {
              "category": "Asia",
              "column-1": "2"
            },
            {
              "category": "Europe",
              "column-1": "17"
            },
            {
              "category": "Africa",
              "column-1": "1"
            }
          ]
        }
      );
    </script>
    <script type="text/javascript">
      AmCharts.makeChart("chartdiv",
        {
          "type": "serial",
          "categoryField": "date",
          "dataDateFormat": "YYYY",
          "theme": "default",
          "categoryAxis": {
            "minPeriod": "YYYY",
            "parseDates": true
          },
          "chartCursor": {
            "enabled": true,
            "animationDuration": 0,
            "categoryBalloonDateFormat": "YYYY"
          },
          "chartScrollbar": {
            "enabled": true
          },
          "trendLines": [],
          "graphs": [
            {
              "bullet": "round",
              "id": "AmGraph-1",
              "title": "Trips abroad",
              "valueField": "column-1"
            },
            {
              "bullet": "square",
              "id": "AmGraph-2",
              "title": "Trips in Lithuania",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "title": "Axis title"
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [
            {
              "id": "Title-1",
              "size": 15,
              "text": "My Trips"
            }
          ],
          "dataProvider": [
            {
              "date": "2012",
              "column-1": 6,
              "column-2": 7
            },
            {
              "date": "2013",
              "column-1": 2,
              "column-2": 3
            },
            {
              "date": "2014",
              "column-1": 4,
              "column-2": 3
            },
            {
              "date": "2015",
              "column-1": 2,
              "column-2": 1
            },
            {
              "date": "2016",
              "column-1": 3,
              "column-2": 2
            },
            {
              "date": "2017",
              "column-1": 4,
              "column-2": 8
            }
          ]
        }
      );
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>

<body class="bgimg">
  <?php include 'header.php' ?>
	<!-- START of markup for breadcrumbs here
	I need to change the color of this and make it change when the page we're at changes
	 -->
	<nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="about_us.php" class="breadcrumb">About Us</a>
        <a href="monica.php" class="breadcrumb">Monica</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->

    
    <body>
      <div class="container">

        <center><h2 class="animated lightSpeedIn">Monica's page</h2></center>

        <center><img id="prof" class="hide-on-med-and-up" src="img/monikos/prof.jpg"/></center>

    <div class="col s12 m7 animated lightSpeedIn">
    <div class="card horizontal">
        <div id="fotop" class="card-image  z-depth-4 hide-on-small-only">
            <img id="prof" src="img/monikos/prof.jpg"/>
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <h5>Hello!</h5>
          <p>My name is Monica Mikalaviciene, and I enjoy meeting new people and finding ways to help them have an uplifting experience. I have had a variety of customer service opportunities, through which I was able to have fewer returned products and increased repeat customers, when compared with co-workers. I am dedicated, outgoing, and a team player. Who could I speak with in your customer service department about your organization’s customer service needs?</p>
          <h5>Email:</h5>
          <p> monikacibirauskaite@yahoo.com</p>
        </div>
        <div class="card-action">
          <a href="https://www.facebook.com/monica.mikalaviciene">My website</a>
        </div>
      </div>
    </div>
  </div>

  <div class="hide-on-small-only" id="chartz" class="col s12">
    <div class="card horizontal">
      <div id="chartdiv1" style="width: 50%; height: 400px; background-color: transparent;" ></div>
      <div id="chartdiv" style="width: 50%; height: 400px; background-color: transparent;" ></div>
    </div>
  </div>

  <center><h4>My recent vocation pics</h4></center>

      <div class="carousel">
            <a class="carousel-item" href="#one!"><img src="img/monikos/1.jpg"></a>
            <a class="carousel-item" href="#two!"><img src="img/monikos/2.jpg"></a>
            <a class="carousel-item" href="#three!"><img src="img/monikos/3.jpg"></a>
            <a class="carousel-item" href="#four!"><img src="img/monikos/4.jpg"></a>
            <a class="carousel-item" href="#five!"><img src="img/monikos/5.jpg"></a>
            <a class="carousel-item" href="#six!"><img src="img/monikos/6.jpg"></a>
            <a class="carousel-item" href="#seven!"><img src="img/monikos/7.jpg"></a>
            <a class="carousel-item" href="#eight!"><img src="img/monikos/8.jpg"></a>
            <a class="carousel-item" href="#nine!"><img src="img/monikos/9.jpg"></a>
            <a class="carousel-item" href="ten!"><img src="img/monikos/10.jpg"></a>
      </div>
    </div>
  </body>










	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>

</html>