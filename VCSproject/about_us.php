<!DOCTYPE html>
<html>

<head>
	<title></title>
	<!-- here we load the CSS files -->
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
  <style>
      #map {
        width: 100%;
        height: 300px;
        background-color: grey;
      }
    </style>

</head>

<body>
<div class="bgimg">
  <!--top navigation bar START -->
      <!-- About Us dropdown markup -->
      <ul id="dropdown1" class="dropdown-content">
        <li><a href="aliaksandr.php">Aliaksandr</a></li>
        <li class="divider"></li>
        <li><a href="rutele.php">Rutele</a></li>
        <li class="divider"></li>
        <li><a href="monica.php">Monica</a></li>
      </ul>

 <nav>
    <div class="nav-wrapper light-blue darken-1">
      <a href="index.php" class="brand-logo"><img src="img/index/logo.png" class="logo brand-logo"></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="index.php">Home</a></li>
        <li><a href="news.php">News</a></li>
        <li>
          <a class="dropdown-button" href="about.php" data-activates="dropdown1" data-toggle="dropdown">About Us<i class="material-icons right"></i>
          </a></li>
      </ul>
    </div>
  </nav>
  <!--top navigation bar END --> 


  <!-- START of breadcrumbs 
  I need to change the color of this and make it change when the page we're at changes
   -->
  <nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="about_us.php" class="breadcrumb">About us</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->


  <!-- "ABOUT US" TABS START HERE -->
  <div class="row">
    <div class="col s12 l8">
      <ul class="tabs">
        <li class="tab col s3"><a class="active blue-text text-darken-2" href="#test1">Team TRYS</a></li>
        <li class="tab col s3"><a class="blue-text text-darken-2" href="#test2">Aliaksanrd</a></li>
        <li class="tab col s3"><a class="blue-text text-darken-2" href="#test3">Monica</a></li>
        <li class="tab col s3"><a class="blue-text text-darken-2" href="#test4">Rutele</a></li>        
        <div class="indicator blue" style="z-index:1">  
        </div>
      </ul>
    </div>

  <!-- "TEAM" TAB -->
    <div id="test1" class="col s12">
      <h4 align="center">MEET OUR TEAM</h4>
      <div class="row">
        <div class="col s10 offset-s1">
          <div class="card">
            <div class="card-image">
              <img src="img/index/rutele_trip.jpg">
                <span class="card-title">A journey of a thousand miles begins with a single step</span>
            </div>
            <div class="card-content">
              <p>We've started April 14th, 2017.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- Aliaksandr -->
    <div id="test2" class="col s12">
      <h4 align="center">MEET OUR TEAM: Aliaksandr</h4>
      <div class="row">
        <div class="col s10 offset-s1">
          <div class="card">
            <div class="card-image">
              <img src="img/index/rutele_trip.jpg">
                <a href="aliaksandr.php" class="col s12 waves-effect waves-light btn-large #ffd54f amber lighten-1">GO TO ALIAKSANDR'S PAGE</a>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- Monica -->
    <div id="test3" class="col s12">
      <h4 align="center">MEET OUR TEAM: Monica</h4>
      <div class="row">
        <div class="col s12 l10 offset-l1">
          <div class="card">
            <div class="card-image">
              <img src="img/index/rutele_trip.jpg">
                <a href="monica.php" class="col s12 waves-effect waves-light btn-large #ffd54f amber lighten-1">GO TO MONICA'S PAGE</a>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- Rutele -->
    <div id="test4" class="col s12">
      <h4 align="center">MEET OUR TEAM: Rutele</h4>
      <div class="row">
        <div class="col s10 offset-s1">
          <div class="card">
            <div class="card-image">
              <img src="img/index/rutele_trip.jpg">
              <a href="rutele.php" class="col s12 waves-effect waves-light btn-large #ffd54f amber lighten-1">GO TO RUTELE'S PAGE</a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

 <!-- end of tabs -->

<!-- MAP AND ADDRESS-->

  <div class="row">
    <div class="col s4 offset-s1">
      <div class="card">
        <div id="map"></div>
          <script>
            function initMap() {
              var sigmute = {lat: 54.71119749, lng: 25.28690871};
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: sigmute
              });
              var marker = new google.maps.Marker({
                position: sigmute,
                map: map
              });
              }
          </script>
          <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtvz89loIXdIJGWpK4IWpSTV4xVvc9U1g&callback=initMap">
          </script>
      </div>
    </div>
  
    <div class="col s6 ">
      <div class="card" style="height: 308px;">
        <div class="card-content">
            <h5>This page was created at:</h5><br/>
            <p>VILNIUS CODING SCHOOL (next door to SIGMUTE)</p>
            <p>Address: Kalvariju g. 125</p>
            <p>Email: info@vilniuscoding.lt</p>
            <p>Phone: +370 606 75071</p> 
            <br/><br/><br/>
            <a class="btn #ffd54f amber lighten-1" href="http://www.vilniuscoding.lt">
            Website</a>
        </div>
      </div>
    </div>
  </div>
</div>

	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>