<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "webas";

$jungtis = new mysqli($servername, $username, $password, $database);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
  

  <!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
<div class="modal-content">
<h4>Add a news story</h4>


<form id="newsform" class="col s12 my-form" accept-charset="UTF-8" action="upload.php" method="post" enctype="multipart/form-data">

  <div class="row">
  <div class="row">
  <div class="input-field col s12">
  <textarea id="Title" name="Title" class="materialize-textarea validate" length="100" required></textarea>
  <label for="Title">Title</label>
  </div>
  </div>
  </div>

  <div class="row">
  <div class="row">
  <div class="input-field col s12">
  <textarea id="Summary" name="Summary" class="materialize-textarea" length="200" required></textarea>
  <label for="Summary">Summary</label>
  </div>
  </div>
  </div>

  <div class="row">
  <div class="row">
  <div class="row">
  <div class="input-field col s12">
  <textarea id="Article" name="Article" class="materialize-textarea" length="2000" required></textarea>
  <label for="Article">Article</label>
  </div>
  </div>
  </div>
  </div>
  <div class="file-field input-field">
  <div class="btn">
  <span>Choose an image</span>
  <input type="file" name="fileToUpload" required>
  </div>
  <div class="file-path-wrapper">
  <input class="file-path validate" type="text">
  </div>
  </div>
  </div>
  <div class="modal-footer">
  <input type="submit" name="submit">
  </div>
</form>
</div>


<div class="fixed-action-btn horizontal click-to-toggle">
    <a id="addstory" class="btn-floating btn-large blue modal-trigger waves-effect waves-light" href="#modal1">
      <i class="material-icons">add</i>
    </a>
  </div>

  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

</body>
</html>