<!DOCTYPE html>
<html>

<head>
  <title>News</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- here we load the CSS files -->
  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">

</head>
</head>
<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "mybase";

$jungtis = new mysqli($servername, $username, $password, $database);
?>

<body class="bgimg">
  <?php include 'header.php' ?>



  <!-- START of markup for breadcrumbs here
  I need to change the color of this and make it change when the page we're at changes
   -->
  <nav>
    <div class="nav-wrapper #ffd54f amber lighten-1">
      <div class="col s12">
        <a href="index.php" class="breadcrumb">Home</a>
        <a href="news.php" class="breadcrumb">News</a>
        <a class="breadcrumb">...</a>
      </div>
    </div>
  </nav>
  <!-- END of breadcrumbs -->

  			<?php 
            $sql = "SELECT * FROM naujienos";
            $pageid = $_GET['id'];
      			$rezultatas = $jungtis->query('SELECT * FROM naujienos WHERE id="'.$pageid.'"');
      			$row = $rezultatas->fetch_assoc();
      			$pav = $row["Title"];
      			$image = $row["Image"];
      			$txt = $row["Summary"];
      			$article = $row["Article"];
      			$image2 = $row["Image2"];
      			$image3 = $row["Image3"];
      		?>
  <h3 class="center-align"><?php echo $pav; ?></h3>
  <center>



   <div id="singleEntry" class="row">
        <div class="col s12 m12 l10 offset-l1">
          <div class="card">
            <div id="one" class="card-image col s12">
            <?php if ($image2==''): ?>
            <div class="carousel carousel-slider col s12 m12 l6 offset-l3 hide-on-small-only">
                <a class="carousel-item" href="#one!"><img src=" <?php echo $image; ?>"></a>
                <a class="carousel-item" href="#two!"><img src=" <?php echo $image; ?>"></a>
                <a class="carousel-item" href="#three!"><img src=" <?php echo $image; ?>"></a>
              </div>
              <img class="hide-on-med-and-up" src="<?php echo $image; ?>">
            <?php else: ?>  
            	<div class="carousel carousel-slider col s12 m12 l6 offset-l3 hide-on-small-only">
    				    <a class="carousel-item" href="#one!"><img src=" <?php echo $image; ?>"></a>
    				    <a class="carousel-item" href="#two!"><img src=" <?php echo $image2; ?>"></a>
    				    <a class="carousel-item" href="#three!"><img src=" <?php echo $image3; ?>"></a>
  				    </div>
              <img class="hide-on-med-and-up" src="<?php echo $image; ?>">  
            <?php endif; ?>
            </div>
              <div class="card-content">
                <p><?php echo $article; ?></p>
              </div>
          </div>
        </div>
      </div>


      <div class="row">
      <div class="col s2"></div>

      <?php 
      			$sql = "SELECT * FROM naujienos where not (id = '".$_GET['id']."') order by rand() LIMIT 2";
            $rezultatas = $jungtis->query($sql);
            
            
            while($irasas = $rezultatas->fetch_assoc()): 
            for ($i=1;$i<2;$i++):
            if($irasas['id']== $_GET['id']): echo '';
            else:
      		?>

      
        <div class="col s12 l4">
          <div class="card horizontal horizontalcard">
            <div class="card-image foto z-depth-4">
            <img class="fotos" src=" <?php echo $irasas['Image']; ?>"/>
            </div>
            <div class="card-stacked">
              <div class="card-content">
              <p class="txt"><?php echo $irasas['Title']; ?></p>
              </div>
              <div class="card-action">
              <a href="newsarticle.php?id=<?php echo $irasas['id']; ?>">Read More</a>
              </div>
            </div>
          </div>
        </div>





      <?php 
      endif;
     endfor;
      endwhile;

      ?>

      </div>
	</center>

	<!-- JavaScript import here -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>

</html>